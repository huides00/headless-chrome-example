const puppeteer = require('puppeteer');

(async () => {

  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://www.facebook.com', { waitUntil: 'networkidle', networkIdleTimeout: 1000 });
  await page.pdf({ path: 'page.pdf', width: '1440px', height: '900px', printBackground: true });
  console.log('Done!')

  browser.close();
})();